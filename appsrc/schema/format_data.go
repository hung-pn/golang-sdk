package schema

type DataFormat struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type DataFormatWeb struct {
	Error   int         `json:"error"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type DataFormatHelpZenDesk struct {
	Error ErrorHelpZendDesk `json:"error"`
	Data  interface{}       `json:"data"`
}

type ErrorHelpZendDesk struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

type DataFormatWap struct {
	Status struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	} `json:"status"`
	Result struct {
		Data interface{} `json:"data"`
	} `json:"result"`
}
