package routers
import (
	"golang-sdk-example/appsrc/handlers"
	"github.com/gin-gonic/gin"
)

type Demo struct {}

func (m *Demo) RouterDemo(r *gin.RouterGroup) {
	groupHelp := r.Group("/demo")
	{
		var demoHandler handlers.Demo
		groupHelp.GET("", demoHandler.GetDemo)
	}
}
