package common

import (
	"golang-sdk-example/appsrc/schema"
)

func Format(status int, message string, data interface{}) schema.DataFormat {
	var result schema.DataFormat
	result.Status = status
	result.Message = message
	result.Data = data
	return result
}

func FormatWeb(errStatus int, message string, data interface{}) schema.DataFormatWeb {
	var result schema.DataFormatWeb
	result.Error = errStatus
	result.Message = message
	result.Data = data
	return result
}

func FormatHelpZenDesk(errStatus int, message string, data interface{}) schema.DataFormatHelpZenDesk {
	var dataEmtpy struct{}
	var result schema.DataFormatHelpZenDesk
	result.Error.Status = errStatus
	result.Error.Message = message
	if data == nil {
		data = dataEmtpy
	}
	result.Data = data
	return result
}
func FormatWap(errStatus int, message string, data interface{}) schema.DataFormatWap {
	var dataEmtpy struct{}
	var result schema.DataFormatWap
	result.Status.Code = errStatus
	result.Status.Message = message
	if data == nil {
		data = dataEmtpy
	}
	result.Result.Data = data
	return result
}
