package common

import (
	"bytes"
	"errors"
	"io/ioutil"
	"net/http"
	"os"
)

type CallService struct {
}

func (mthis *CallService) RequestAnotherService(urlString string, dataPostArray ...string) (string, error) {
	os.Setenv("HTTP_PROXY", "")
	var token string
	var authorization string
	var dataPost string
	// var proxyUrlString string
	var req *http.Request
	err := errors.New("")
	hc := &http.Client{}
	if len(dataPostArray) == 0 {
		req, err = http.NewRequest("GET", urlString, nil)
		if err != nil {
			return "", err
		}
	} else {
		dataPost = dataPostArray[0]
		if dataPost == "" {
			req, err = http.NewRequest("GET", urlString, nil)
			if err != nil {
				return "", err
			}
		} else {
			req, err = http.NewRequest("POST", urlString, bytes.NewReader([]byte(dataPost)))
			if err != nil {
				return "", err
			}

		}

		if len(dataPostArray) > 1 {
			if dataPostArray[1] != "" {
				token = dataPostArray[1]
			}
		}

		if len(dataPostArray) > 2 {
			if dataPostArray[2] != "" {
				authorization = dataPostArray[2]
			}
		}
	}
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	if token != "" {
		req.Header.Set("x-token", token)
	}

	if authorization != "" {
		req.Header.Set("Authorization", authorization)
	}
	// req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36")

	resp, err := hc.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), err
}
