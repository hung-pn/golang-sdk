package common

import (
	"golang-sdk-example/appsrc/resprovider"
	"github.com/gomodule/redigo/redis"
)

type RedisCommon struct {}

// ==========================================================================
func (m *RedisCommon) connect() redis.Conn {
	return resprovider.GetInstance().Redis()
}

func (m *RedisCommon) SetHSET(key, field, value string) {
	c := m.connect()
	defer c.Close()
	c.Do("HSET", key, field, value)
}

func (m *RedisCommon) GetHSET(key, field string) (string, error) {
	c := m.connect()
	defer c.Close()
	s, err := redis.String(c.Do("HGET", key, field))
	return s, err
}

func (m *RedisCommon) DeleteKeyHSET(key, field string) error {
	c := m.connect()
	defer c.Close()

	_, err := c.Do("HDEL", key, field)
	return err
}

func (m *RedisCommon) GetString(key string) (string, error) {
	c := m.connect()
	defer c.Close()

	s, err := redis.String(c.Do("GET", key))
	return s, err
}

func (m *RedisCommon) GetInt(key string) (int, error) {
	c := m.connect()
	defer c.Close()

	return redis.Int(c.Do("GET", key))
}

func (m *RedisCommon) SetString(key string, expire int, value string) {
	c := m.connect()
	defer c.Close()

	// TODO check and store error log
	if expire <= 0 {
		c.Do("SET", key, value)
	} else {
		c.Do("SETEX", key, expire, value)
	}
}

func (m *RedisCommon) DeleteKey(key string) error {
	c := m.connect()
	defer c.Close()

	_, err := c.Do("DEL", key)
	return err
}
