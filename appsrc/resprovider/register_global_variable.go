package resprovider

import (
	sdms "gitlab.sendo.vn/core/golang-sdk"
)

type RegisterGlobalVariableService interface {
	sdms.Service
	RegisterGlobalVariableInterface
}

type RegisterGlobalVariableInterface interface {
}
type myRegisterGlobalVariable struct {
}

func NewRegisterGlobalVariableService() RegisterGlobalVariableService {
	return &myRegisterGlobalVariable{}
}

func (m *myRegisterGlobalVariable) InitFlags() {
}

func (m *myRegisterGlobalVariable) Configure() error { return nil }

func (m *myRegisterGlobalVariable) Cleanup() {}