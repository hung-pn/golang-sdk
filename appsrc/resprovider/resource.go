package resprovider

import (
	"log"
	"path"
	"runtime"
	storage "golang-sdk-example/appsrc/storage/es"
	"github.com/olivere/elastic"
	"github.com/gomodule/redigo/redis"
	sdms "gitlab.sendo.vn/core/golang-sdk"
	"gitlab.sendo.vn/core/golang-sdk/slog"
	"gitlab.sendo.vn/core/golang-sdk/sredigo"
	"gitlab.sendo.vn/core/golang-sdk/smgo"
	"github.com/globalsign/mgo"
)

type Logger slog.Logger

// provide interface to access remote resources
type ResourceProvider interface {
	// Absolute dir to app root
	GetAppRoot() string
	// get console Logger
	Logger(prefix string) Logger
	// get file logger
	Flogger(prefix string) Logger
	// get a redis connection from pool
	Redis() redis.Conn
	Session(opts ...smgo.Option) *mgo.Session
	SessionCore(opts ...smgo.Option) *mgo.Session

	EsClient() *elastic.Client

	//register global variable
	RegisterGlobalVariableInterface
}

var rp ResourceProvider

func InitResourceProvider(app sdms.Application, cron ...bool) {
	isCron := false
	if len(cron) > 0 {
		isCron = cron[0]
	}
	if rp != nil {
		app := rp.(*myResourceProvider).app
		if !app.IsShutdown() {
			log := rp.Logger("resprovider")
			log.Fatal("InitResourceProvider can not called twice!")
		}
	}
	rp = newRP(app, isCron)
}

// get ResourceProvider instace
func GetInstance() ResourceProvider {
	if rp == nil {
		log.Fatal("InitResourceProvider is not called yet!")
	}
	return rp
}

// private implement
type myResourceProvider struct {
	app sdms.Application
	// console log
	clog slog.LoggerService
	rootDir string
	// file logging
	flog slog.LoggerService
	sredigo.RedigoService
	sredigoCore sredigo.RedigoService
	smgo.MgoService
	smgoCore smgo.MgoService
	storage.ElasticSearch
	esClient *elastic.Client
	RegisterGlobalVariableService
}

func newRP(app sdms.Application, isCron bool) *myResourceProvider {
	rp := &myResourceProvider{}
	rp.app = app

	// redis
	rp.RedigoService = sredigo.NewRedigo(&sredigo.RedigoConfig{
		App: app,
	})
	app.RegService(rp.RedigoService)

	rp.MgoService = smgo.NewMgoService(&smgo.MgoConfig{
		App:           app,
		DefaultDBName: "test",
	})
	app.RegService(rp.MgoService)

	rp.smgoCore = smgo.NewMgoService(&smgo.MgoConfig{
		App:           app,
		FlagPrefix:    "event-",
		DefaultDBName: "test",
	})
	app.RegService(rp.smgoCore)

	//register env
	rp.RegisterGlobalVariableService = NewRegisterGlobalVariableService()
	rp.ElasticSearch = storage.NewES( "es", "")
	app.RegService(rp.RegisterGlobalVariableService)
	return rp
}

func (m *myResourceProvider) GetAppRoot() string {
	if m.rootDir == "" {
		_, file, _, _ := runtime.Caller(0)
		m.rootDir = path.Dir(path.Dir(path.Dir(file)))
	}
	return m.rootDir
}

func (m *myResourceProvider) Logger(prefix string) Logger {
	return m.clog.GetLogger(prefix)
}

func (m *myResourceProvider) Flogger(prefix string) Logger {
	return m.flog.GetLogger(prefix)
}

func (m *myResourceProvider) Redis() redis.Conn {
	return m.RedigoService.Get()
}

func (m *myResourceProvider) SessionCore( opts ...smgo.Option) (s *mgo.Session) {
	db, _ := m.smgoCore.DB(opts...)
	return db.Session
}

func (m *myResourceProvider) EsClient() *elastic.Client {
	return m.ElasticSearch.EsClient()
}