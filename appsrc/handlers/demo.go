package handlers

import (
	"golang-sdk-example/appsrc/common"
	"golang-sdk-example/appsrc/services"
	"github.com/gin-gonic/gin"
)


type Demo struct {}

func (m *Demo) GetDemo(c *gin.Context) {
	demoServices := services.NewDemoService()
	data, _ := demoServices.GetDemo()
	c.JSON(200, common.FormatWeb(0, "Successful", data))
}