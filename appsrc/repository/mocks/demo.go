package mocks

import (
	mock "github.com/stretchr/testify/mock"
	schema "golang-sdk-example/appsrc/schema"
)

type Demo struct {
	mock.Mock
}

func (_m *Demo) Find() ([]schema.Demo, error) {
	ret := _m.Called()

	var r0 []schema.Demo
	if rf, ok := ret.Get(0).(func() []schema.Demo); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).([]schema.Demo)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}