package repository

import (
	"golang-sdk-example/appsrc/schema"
	dl "golang-sdk-example/appsrc/repository/data_layer"
	"golang-sdk-example/appsrc/resprovider"
	"github.com/globalsign/mgo/bson"
	"os"
)

type Demo interface {
	Find() ([]schema.Demo, error)
}

type demoRepository struct {
	mgo dl.MgoDataLayer
	rds dl.RedisDataLayer
	es  dl.EsDataLayer
}

func NewDemoRepository() Demo {
	return &demoRepository{
		mgo: dl.NewMgoDataLayer(resprovider.GetInstance().Session(),"demo"),
		rds: dl.NewRedisDataLayer(),
		es:  dl.NewEsDataLayer(resprovider.GetInstance().EsClient(), os.Getenv("ES_INDEX"), os.Getenv("ES_TYPE")),
	}
}

func (m *demoRepository) Find() ([]schema.Demo, error) {
	var demos []schema.Demo
	err := m.mgo.FindAll(bson.M{}, &demos, 1, 10, nil)
	return demos, err
}