package data_layer

import (
	"github.com/gomodule/redigo/redis"
	"golang-sdk-example/appsrc/resprovider"
)

type RedisDataLayer interface {
	SetHSET(key, field, value string)
	GetHSET(key, field string) (string, error)
	DeleteKeyHSET(key, field string) error
	GetString(key string) (string, error)
	SetString(key string, expire int, value string)
	DeleteKey(key string) error
}

type redisDataLayer struct {}

func NewRedisDataLayer() *redisDataLayer {
	return &redisDataLayer{}
}

// ==========================================================================
// if mutiple connect Redis. Pls switch or edit sdk return session
func (m *redisDataLayer) connect() redis.Conn {
	return resprovider.GetInstance().Redis()
}

func (m *redisDataLayer) SetHSET(key, field, value string) {
	c := m.connect()
	defer c.Close()
	c.Do("HSET", key, field, value)
}

func (m *redisDataLayer) GetHSET(key, field string) (string, error) {
	c := m.connect()
	defer c.Close()
	s, err := redis.String(c.Do("HGET", key, field))
	return s, err
}

func (m *redisDataLayer) DeleteKeyHSET(key, field string) error {
	c := m.connect()
	defer c.Close()

	_, err := c.Do("HDEL", key, field)
	return err
}

func (m *redisDataLayer) GetString(key string) (string, error) {
	c := m.connect()
	defer c.Close()

	s, err := redis.String(c.Do("GET", key))
	return s, err
}

func (m *redisDataLayer) GetInt(key string) (int, error) {
	c := m.connect()
	defer c.Close()

	return redis.Int(c.Do("GET", key))
}

func (m *redisDataLayer) SetString(key string, expire int, value string) {
	c := m.connect()
	defer c.Close()

	// TODO check and store error log
	if expire <= 0 {
		c.Do("SET", key, value)
	} else {
		c.Do("SETEX", key, expire, value)
	}
}

func (m *redisDataLayer) DeleteKey(key string) error {
	c := m.connect()
	defer c.Close()

	_, err := c.Do("DEL", key)
	return err
}