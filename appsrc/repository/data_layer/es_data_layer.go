package data_layer

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/olivere/elastic"
	"time"
)

var (
	TIME_LOCATION, _ = time.LoadLocation("Asia/Ho_Chi_Minh")
)

type EsDataLayer interface {
	Upsert(id string, something interface{}) error
	Update(id string, something interface{}) error
	UpsertBulk(ids []string, something []interface{}) (*elastic.BulkResponse, error)
	Find(bq *elastic.BoolQuery, offset, limit int, orderBy string, order string) (*elastic.SearchResult, error)
	UpdateBulk(ids []string, something []interface{}) (*elastic.BulkResponse, error)
}

type esDataLayer struct {
	s         *elastic.Client
	indexName string
	typeName  string
}

func NewEsDataLayer(s *elastic.Client, indexName string, typeName string) EsDataLayer {
	return &esDataLayer{s: s, indexName: indexName, typeName: typeName}
}

func (dl *esDataLayer) Upsert(id string, something interface{}) error {
	ctx := context.Background()
	_, err := dl.s.Index().
		Index(dl.indexName).Type(dl.typeName).Id(id).
		BodyJson(something).
		Do(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (dl *esDataLayer) Update(id string, something interface{}) error {
	ctx := context.Background()
	_, err := dl.s.Update().
		Index(dl.indexName).Type(dl.typeName).Id(id).
		Doc(something).
		Do(ctx)
	if err != nil {
		return err
	}
	return nil
}

func DebugsEs(query *elastic.BoolQuery) {
	src, err := query.Source()
	if err != nil {
		panic(err)
	}
	data, err := json.MarshalIndent(src, "", "  ")
	if err != nil {
		panic(err)
	}
	fmt.Println(string(data))
}

func (dl *esDataLayer) UpsertBulk(ids []string, something []interface{}) (*elastic.BulkResponse, error) {
	ctx := context.Background()
	bulkRequest := dl.s.Bulk()
	for i, v := range ids {
		req := elastic.NewBulkIndexRequest().Index(dl.indexName).Type(dl.typeName).Id(v).Doc(something[i])
		bulkRequest.Add(req)
	}
	bulkResponse, err := bulkRequest.Do(ctx)

	return bulkResponse, err
}

func (dl *esDataLayer) UpdateBulk(ids []string, something []interface{}) (*elastic.BulkResponse, error) {
	ctx := context.Background()
	bulkRequest := dl.s.Bulk()
	for i, v := range ids {
		req := elastic.NewBulkUpdateRequest().Index(dl.indexName).Type(dl.typeName).Id(v).Doc(something[i])
		bulkRequest.Add(req)
	}
	bulkResponse, err := bulkRequest.Do(ctx)

	return bulkResponse, err
}

func (dl *esDataLayer) Find(bq *elastic.BoolQuery, offset, limit int, orderBy string, order string) (*elastic.SearchResult, error) {
	ctx := context.Background()
	ascending := true
	if order == "desc" {
		ascending = false
	}

	var searchService *elastic.SearchService

	searchService = dl.s.Search().
		Index(dl.indexName).
		Type(dl.typeName).
		Query(bq).
		From(offset).Size(limit).
		Pretty(true)

	if orderBy != "" {
		searchService = searchService.Sort(orderBy, ascending)
	}
	return searchService.Do(ctx)
	//DebugsEs(bq)
}
