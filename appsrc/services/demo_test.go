package services

import (
	"testing"
	"golang-sdk-example/appsrc/schema"
	"github.com/stretchr/testify/assert"
	"golang-sdk-example/appsrc/repository/mocks"
)

func TestDemo_GetDemo(t *testing.T) {
	demoMock := new(mocks.Demo)
	
	type input struct {
		demos []schema.Demo
		demoRepo demoService
	}
	tests := []struct {
		name  string
		input input
		want  bool
	}{
		{
			name: "Test Get Demo",
			input: input{demos: []schema.Demo{
						{
							Id: 1,
						},
						{
							Id: 2,
						},
					},
				},
			want: true,
		},
	}
	for _, item := range tests {
		demoMock.On("Find").Return(item.input.demos, nil)

		demo := demoService{
			DemoRepo: demoMock,
		}

		result, err := demo.GetDemo()
		assert := assert.New(t)
		assert.Nil(err)
		assert.Equal(result, item.input.demos, "they should be equal")
	}
}