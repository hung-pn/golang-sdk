package services

import (
	"golang-sdk-example/appsrc/schema"
	"golang-sdk-example/appsrc/repository"
)

type DemoService interface {
	GetDemo() ([]schema.Demo, error)
}

type demoService struct {
	DemoRepo    	repository.Demo
}

func NewDemoService() DemoService {
	return &demoService{
		DemoRepo:  repository.NewDemoRepository(),
	}
}

func (m *demoService) GetDemo() ([]schema.Demo, error) {
	return m.DemoRepo.Find()
}