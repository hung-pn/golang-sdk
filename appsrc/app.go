package appsrc

import (
	"golang-sdk-example/appsrc/resprovider"
	"os"
	"strings"

	"gitlab.sendo.vn/core/golang-sdk"
	"gitlab.sendo.vn/core/golang-sdk/sgin"
	"gitlab.sendo.vn/core/golang-sdk/ssd"
)

const SERVICE_NAME = "golang-sdk-example"

func CreateApp(args []string, job string) sdms.Application {
	name := ""
	if len(os.Args) >= 2 {
		name = strings.Join(os.Args[:2], " ")
	}
	app := sdms.NewApp(&sdms.AppConfig{
		Name: name,
		Args: args,
	})

	sd := ssd.NewConsul(app)
	app.RegService(sd)
	if job == "" {
		resprovider.InitResourceProvider(app)
	} else {
		resprovider.InitResourceProvider(app, true)
	}

	var main sdms.RunnableService

	switch job {
	default:
		ginCnf := sgin.Config{
			App:         app,
			SD:          sd,
			ServiceName: SERVICE_NAME + "_http",
			FlagPrefix:  "http-",
			RegFunc:     registerHandlers,
		}
		ginSvc, _ := sgin.New(&ginCnf)
		main = sdms.NewServiceMux(ginSvc)

	}

	app.RegMainService(main)

	return app
}
