package storage

import (
	"flag"
	"fmt"
	"github.com/olivere/elastic"
	"gitlab.sendo.vn/core/golang-sdk/new/logger"
	"log"
	"os"
)

type ESOpt struct {
	Prefix string
	Uri    string
}

type ElasticSearch interface {
	EsClient() *elastic.Client
	InitFlags()
	Configure() error
	Cleanup()
}


type myES struct {
	name   string
	client *elastic.Client
	logger logger.Logger
	*ESOpt
}

func NewES(name, flagPrefix string) ElasticSearch {
	 esInit := &myES{
		name: name,
		ESOpt: &ESOpt{
			Prefix: flagPrefix,
			//Uri: Uri,
		},
	}
	return esInit
}

func (es *myES) GetPrefix() string {
	return es.Prefix
}

func (es *myES) isDisabled() bool {
	return es.Uri == ""
}


func (m *myES) InitFlags() {
	flag.StringVar(&m.Uri, "es-uri", "", "domain Es")
}


func (m *myES) Cleanup() {}

func (es *myES) Configure() error {

	fmt.Println("Connecting to Elastic Search at ", es.Uri, "...")

	client, err := elastic.NewClient(
		elastic.SetURL(es.Uri),
		elastic.SetInfoLog(log.New(os.Stdout, "ELASTIC ", log.LstdFlags)),
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false))

	if err != nil {
		es.logger.Error("Cannot connect to Elastic Search. ", err.Error())
		fmt.Println("Cannot connect to Elastic Search. ", err.Error())

		return err
	}

	// Connect successfully, assign client
	es.client = client
	return nil
}

func (es *myES) Name() string {
	return es.name
}

func (es *myES) EsClient() *elastic.Client {
	return es.client
}

func (es *myES) Run() error {
	return es.Configure()
}

func (es *myES) Stop() <-chan bool {
	// if es.client != nil {
	// 	es.client.Close()
	// }

	c := make(chan bool)
	go func() { c <- true }()
	return c
}
