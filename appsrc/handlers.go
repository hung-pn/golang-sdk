package appsrc

import (
	"golang-sdk-example/appsrc/routers"
	"github.com/gin-gonic/gin"
	"github.com/gin-contrib/gzip"
)

func registerHandlers(r *gin.Engine) {
	r.Use(gzip.Gzip(gzip.DefaultCompression))
	r.GET("/ping", pingHandler)
	r.GET("/ip", clientIpHandler)

	var routerDemo 	routers.Demo

	groupPublic := r.Group("")
	{
		routerDemo.RouterDemo(groupPublic)
	}
}

func clientIpHandler(c *gin.Context) {
	c.String(200, c.ClientIP()+"\n")
}
func pingHandler(c *gin.Context) {
	c.String(200, "pong\n")
}