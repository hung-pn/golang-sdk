module golang-sdk-example

go 1.15

require (
	github.com/facebookgo/flagenv v0.0.0-20160425205200-fcd59fca7456 // indirect
	github.com/gin-contrib/gzip v0.0.1
	github.com/gin-gonic/gin v1.4.0
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/hashicorp/consul/api v1.2.0 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/mailru/easyjson v0.0.0-20190626092158-b2ccc519800e // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/olivere/elastic v6.2.23+incompatible
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/x-cray/logrus-prefixed-formatter v0.5.2 // indirect
	gitlab.sendo.vn/core/golang-sdk v2.2.1+incompatible
)

replace github.com/ugorji/go v1.1.4 => github.com/ugorji/go/codec v0.0.0-20190204201341-e444a5086c43
